
#include <stdio.h>
#include <qt4/Qt/qfile.h>
#include <qt4/Qt/qdatastream.h>

void __kick_newline(char* str)
{
    if (!str)
	return;

    for (char* c=str; *c; c++)
	if ( ((*c)==0x10) || ((*c)==0x13) || ((*c)=='\n') || ((*c)=='\r') )
	    (*c)='+';
}

void outvar_str(char* name, QString str)
{
    QByteArray ba = str.toAscii();
    char* value = ba.data();
    __kick_newline(value);
    printf("%s: %s\n", name, value);
}
    
void outvar_int(const char* name, signed char c)
{
    int i = c;
    printf("%s: %d\n", name, i);
}

void outvar_bool(const char* name, signed char c)
{
    if (c)
	printf("%s: true\n", name);
    else
	printf("%s: false\n", name);
}

void readConfig(const char* fn)
{
    QFile f(fn);
    f.open(QIODevice::ReadOnly);
    QDataStream stream(&f);
    QString str;
    QString version;
    signed char qi8;

    stream >> version;
    if (version == "0.1")
    {
	printf("# loading from 0.1 config file: %s\n", fn);
	stream >> qi8;	outvar_int("traces", qi8);
	stream >> str;	outvar_str("lfo-shaping", str);
	stream >> str;	outvar_str("frequency-shaping", str);
	stream >> str;	outvar_str("frequency-low", str);
	stream >> str;	outvar_str("frequency-high", str);
	stream >> qi8;	outvar_bool("stereo", qi8);
	stream >> str;	outvar_str("brainwave-frequency", str);
	stream >> qi8;	outvar_bool("independent-stereo", qi8);
	stream >> str;	outvar_str("playrate", str);
	stream >> qi8;	outvar_bool("use-dsp", qi8);
	stream >> str;	outvar_str("output-filename", str);
	stream >> str;	outvar_str("comments", str);
    }      
    else
    {
	fprintf(stderr, "## unsupported version\n");
    }
}

int main(int argc, char* argv[])
{
    if (argc<2)
    {
	fprintf(stderr,"config_convert <old-filename>\n");
	exit(-1);
    }
    
    readConfig(argv[1]);
}
