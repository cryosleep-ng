
VERSION = 0.3.1.0
DESTDIR    ?= /usr/local/
RM         ?= rm -f
CXX        ?= c++
PKG_CONFIG ?= pkg-config

########################### (auto)configuration ############################

QTCORE_CFLAGS?=`$(PKG_CONFIG) --cflags QtCore`
QTCORE_LIBS?=`$(PKG_CONFIG) --libs QtCore`

LIBM_CFLAGS?=
LIBM_LIBS?="-lm"

CFLAGS?=-march=athlon64 -msse2 -msse3 -mmmx -m3dnow -ffast-math -fpmath=sse -maccumulate-outgoing-args

#############################################################################

bin: cryosleep

#############################################################################
# Rules
#############################################################################

clean: 
	$(RM) a.out core *.o cryosleep config_convert

cryosleep: cryosleep.c
	$(CC) $< -o $@ $(CFLAGS) $(LIBM_CFLAGS) $(LIBM_LIBS)

config_convert: config_convert.cpp
	$(CXX) $< -o $@ $(CFLAGS) $(QTCORE_LIBS) $(QTCORE_CFLAGS) 

create-newconf:	config_convert
	mkdir -p conf
	for i in air busy field melt perfect resair sleep space trance water ; do \
	    ./config_convert tubes/$$i.cryo > conf/$$i.conf ; \
	done

dump:
	@echo "CC=$(CC)"
	@echo "CFLAGS=$(CFLAGS)"
	@echo "CXX=$(CXX)"
	@echo "CXXFLAGS=$(CXXFLAGS)"
