
#define _GNU_SOURCE
#define VERSION "0.3.1.0"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <linux/soundcard.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>

typedef enum { false = 0, true = 1 } bool;

typedef union
{
  struct 
  {
    signed short int left;
    signed short int right;
  } leftright;
} longtrick;

/* output device stuff */

int  output_fd = -1;
bool output_stop = true;

#define wavrate   ((double)44100)
#define pi        ((double)3.141596)
#define barksize  (24)
#define bufsize   (8000)
longtrick buffer[bufsize];
static double barkbounds[barksize+1] =
  {
    0,100,200,300,400,510,630,770,920,1080,1270,1480,1720,2000,2380,2700,
    3150,3700,4400,5300,6400,7700,9500,12000,15500	
  };

static double perfectspectrum[barksize] = 
  {7.48973,
   8.60855,
   7.30108,
   6.08735,
   5.05958,
   4.22398,
   3.55564,
   2.84894,
   1.99757,
   1.37338,
   0.814002,
   0.479557,
   -0.125073,
   -0.522579,
   -0.939505,
   -1.68769,
   -2.2336,
   -3.15377,
   -4.26618,
   -5.23179,
   -6.09273,
   -7.06462,
   -8.35773,
   -10.1641};

static double perfect_amp(double freq)
{
  int i;
  for(i = 0 ; i < barksize ; i ++)
    if (freq>=barkbounds[i] && freq < barkbounds[i+1])
      {
	double left = barkbounds[i];
	double right = barkbounds[i+1];
	double center = (left+right)/2.0;
	double y1 = 0;
	double y2 = 0;
	if (freq<=center)
	  {
	    if (i>0) y1 = perfectspectrum[i-1];
	    y2 = perfectspectrum[i];
	  }
	else
	  {
	    y1 = perfectspectrum[i];
	    if (i<barksize) y1 = perfectspectrum[i+1];
	  }
	double answer = y1+(freq-left)*(y2-y1)/(center-left);
	answer = exp10(answer/10.0);
	printf("freq %g will be played at volume %g\n",freq,answer);
	return answer;
      }
  return 0;
}

static double *lfo_frequencies[barksize];
static double *lfo_phases[barksize];
static double *lfo_amps[barksize];
static double *std_frequencies[barksize];
static double *std_phases[barksize];
static double std_amps[barksize];
static double scaling_factor = 2.0*pi/wavrate;

const char* n_comments                    = "";
const char* n_playrate                    = "44100";
const char* n_lfo_shaping                 = "";
const char* n_freq_shaping                = "";
const char* n_brainwave_freq              = "100";
const char* n_output_filename             = "cryosleep-generation.raw";
const char* n_dsp_filename                = "/dev/dsp";
static bool        n_independent_stereo_channels = false;
static double      n_freq_lo;
static double      n_freq_hi;
static int         n_traces                      = 1;
static bool        n_use_dsp                     = false;
static bool        n_stereo                      = false;
static bool        n_perfect                     = false;

bool __atob(const char* v)
{
    if (!v)
	return false;
    if (strcmp(v,"true")==0)
	return true;
    if (strcmp(v,"yes")==0)
	return true;
	
    return false;
}

void handle_value(const char* name, const char* value)
{
	if ((!name)||(!strlen(name)))
	    return;

	if (strcmp(name,"lfo-shaping")==0)
	    n_lfo_shaping = strdup(value);
	else if (strcmp(name, "traces")==0)
	    n_traces = atoi(value);
	else if (strcmp(name,"frequency-shaping")==0)
	    n_freq_shaping = strdup(value);
	else if (strcmp(name,"frequency-low")==0)
	    n_freq_lo = atof(value);
	else if (strcmp(name,"frequency-high")==0)
	    n_freq_hi = atof(value);
	else if (strcmp(name,"dsp-filename")==0)
	    n_dsp_filename = strdup(value);
	else if (strcmp(name,"output-filename")==0)
	    n_output_filename = strdup(value);
	else if (strcmp(name,"comments")==0)
	    n_comments = strdup(value);
	else if (strcmp(name,"brainwave-frequency")==0)
	    n_brainwave_freq = strdup(value);
	else if (strcmp(name,"playrate")==0)
	    n_playrate = strdup(value);
	else if (strcmp(name,"use-dsp")==0)
	    n_use_dsp = __atob(value);
	else if (strcmp(name,"stereo")==0)
	    n_stereo = __atob(value);
	else if (strcmp(name,"independent-stereo")==0)
	    n_independent_stereo_channels = __atob(value);
	else
	    printf("%s=\"%s\"\n", name, value);    
}

void read_line(char* buffer)
{
	char* fieldname;
	char* value;
    
	// kick off comments
	char* c;
	for(c=buffer; *c; c++)
	    if ((*c)=='#')
	        *c=0;
    
	// now skip leading spaces 
	while ((*buffer==' ')||(*buffer=='\n')||(*buffer=='\r')||(*buffer=='\t'))
	    *buffer++;
		
        // find the ':'
	char* colon = strchr(buffer,':');
	if (!colon)
	{
	    if (strlen(buffer))
	    {
	        fprintf(stderr,"Maybe broken line: \"%s\"\n", buffer);
	    }
	    return;
	}
	
	/* skip off follwing spaces in variable name */
	char* backptr;
	backptr = colon;
	while ((backptr>buffer)&&((*backptr==' ')||(*backptr=='\t')||(*backptr=='\n')||(*backptr=='\r')))
	    backptr--;
	*backptr = 0;
	
	/* now get the field name */
	fieldname = buffer;
	
	/* jump to value and skip spaces there */
	buffer = backptr+1;
	while ((*buffer==' ')||(*buffer=='\n')||(*buffer=='\r')||(*buffer=='\t'))
	    *buffer++;
	
	if (strlen(buffer))
	{
	    backptr=buffer+strlen(buffer)-1;
	    while ((backptr>buffer)&&((*backptr==' ')||(*backptr=='\t')||(*backptr=='\n')||(*backptr=='\r')))
		backptr--;
	    backptr++;
	    *backptr=0;
	    value = buffer;
	}
	else
	    value = "";
	    
	handle_value(fieldname,value);
}

void read_cf(const char* fn)
{
    char buffer[8192];

    FILE* fp = fopen(fn,"r");
    while (!feof(fp))
    {
	if (fgets(buffer, sizeof(buffer)-1, fp))
	    read_line(buffer);
    }
}

int dsp_open(const char* device)
{
  int p;
  output_fd=open(device,O_WRONLY);
  if (output_fd==-1)
    {
      fprintf(stderr,"Could not open dsp device \"%s\"\n",device);
      exit(50);
    }
  
  // set dsp parameters
  p=AFMT_S16_LE;
  if (ioctl(output_fd,SNDCTL_DSP_SETFMT,&p)==-1)
    {
      fprintf(stderr,"dsp: setting dsp to standard 16 bit failed\n");
      exit(50);
    }
  p=2;
   if (ioctl(output_fd,SNDCTL_DSP_CHANNELS,&p)==-1)
    {
      fprintf(stderr,"dsp: setting dsp to 2 channels failed\n");
      exit(50);
    }
  p=11025;
  if (ioctl(output_fd,SNDCTL_DSP_SPEED,&p)==-1)
    {
      printf("dsp: setting dsp speed (%d) failed\n",p++);
      if (ioctl(output_fd,SNDCTL_DSP_SPEED,&p)==-1)
	{
	  printf("dsp: setting dsp speed (%d) failed\n",p);
	  p+=2;
	  if (ioctl(output_fd,SNDCTL_DSP_SPEED,&p)==-1)
	    {
	      printf("dsp: setting dsp speed (%d) failed\n",p);
	      exit(50);
	    }
	}
    }
  return 0;
}

#define choose(lo,hi)	((double)(random()%10000)*(hi-lo)/10000.0+lo)
#define mean(lo,hi)	((lo+hi)/2)

void file_open(const char * name)
{
  output_fd = open(name,O_CREAT|O_WRONLY|O_TRUNC,S_IRUSR|S_IWUSR);
  assert(output_fd!=-1);
}

void open_output()
{
  assert(output_fd == -1);
  if (n_use_dsp)
    dsp_open(n_dsp_filename);
  else
    file_open(n_output_filename);
}

void run_cryo_loop()
{
  double pos = 0.0, maximum = 0.0, phase, freq;
  int i, j, k = 0, track;
  
  output_stop=false;
  
  srandom(time(NULL));
  open_output();
  
  char yeah[1024];
  double brainwave = 0;
  if (n_stereo)
    brainwave = atof(n_brainwave_freq);

  double playrate     = playrate = atof(n_playrate);

  {
    double lfo_shaping  = atof(n_lfo_shaping);
    double freq_shaping = atof(n_freq_shaping);
  
    for ( track = 0 ; track < barksize ; track ++)
    {
      lfo_frequencies[track] = (double*)malloc(sizeof(double)*n_traces);
      lfo_phases[track]      = (double*)malloc(sizeof(double)*n_traces);
      lfo_amps[track]        = (double*)malloc(sizeof(double)*n_traces);
      std_frequencies[track] = (double*)malloc(sizeof(double)*n_traces);
      std_phases[track]      = (double*)malloc(sizeof(double)*n_traces);
    }

     for ( track = 0 ; track < barksize ; track ++ ) 
	for ( i = 0 ; i < n_traces ; i ++ ) 
        {
	    double down = barkbounds [ track ] ;
	    double up   = barkbounds [ track+1 ] ;
	    freq = choose ( n_freq_lo,n_freq_hi ) ;
	    lfo_phases      [ track ] [ i ] = choose ( 0,2.0*pi ) ;
	    lfo_frequencies [ track ] [ i ] = freq*scaling_factor;
	    lfo_amps        [ track ] [ i ] = sqrt ( pow ( freq , - lfo_shaping ) ) ;
	    std_phases      [ track ] [ i ] = choose ( 0,2.0*pi ) ;
	    double trace_freq =
		std_frequencies [ track ] [ i ] = choose ( down,up ) *scaling_factor;
	    if (n_perfect)
		std_amps        [ track ] = perfect_amp(mean ( down,up ));
	    else
	    std_amps        [ track ] = sqrt ( pow ( mean ( down,up ) , - freq_shaping ) ) ;
	}
  }

  while ( ! output_stop )
    {
      for ( j = 0 ; j < bufsize ; j++ )
	{
	  register double left;
	  register double right;
	  register double * phases;
	  register double * freqs;
	  register double * amps;
	  if (n_independent_stereo_channels)
	    {
	      register double volume_left;
	      register double volume_right;
	      // panner version
	      for ( track = left = right = 0 ; track < barksize ; track ++ )
		{
		  phases = lfo_phases      [ track ] ;
		  freqs  = lfo_frequencies [ track ] ;
		  amps   = lfo_amps        [ track ] ;
		  volume_left = 0;
		  volume_right = 0;
		  for( i = 0; i < n_traces /2 ; i ++ )
		    {
		      volume_left  += sin ( phases [ i ] + freqs [ i ] * pos ) * amps [ i ] ;
		    }
		  for( i = n_traces/2; i < n_traces ; i ++ )
		    {
		      volume_right  += sin ( phases [ i ] + freqs [ i ] * pos ) * amps [ i ] ;
		    }
#ifdef firefix
		  volume_left = 1.0 - abs(volume_left);
		  volume_right = 1.0 - abs(volume_right);
#endif
		  phases  = std_phases      [ track ] ;
		  freqs   = std_frequencies [ track ] ;
		  volume_left  *= std_amps  [ track ] ;
		  volume_right *= std_amps  [ track ] ;
		  for(i = 0 ; i < n_traces ; i ++)
		    {
		      phase  = phases [ i ] ;
		      freq   = freqs  [ i ] ;
		      left  += volume_left  * sin ( phase + pos * freq ) ;
		      freq  += brainwave    * scaling_factor;
		      right += volume_right * sin ( phase + pos * freq ) ;
		    }
		}
	    }
	  else
	    {
	      // brainwave version
	      register double volume;
	      for ( track = left = right = 0 ; track < barksize ; track ++ )
		{
		  phases = lfo_phases      [ track ] ;
		  freqs  = lfo_frequencies [ track ] ;
		  amps   = lfo_amps        [ track ] ;
		  for( i = volume = 0 ; i < n_traces ; i ++ )
		    {
		      volume  += sin ( phases [ i ] + freqs [ i ] * pos ) * amps [ i ] ;
		    }
		  phases  = std_phases      [ track ] ;
		  freqs   = std_frequencies [ track ] ;
		  volume *= std_amps        [ track ] ;
		  for(i = 0 ; i < n_traces ; i ++)
		    {
		      phase  = phases [ i ] ;
		      freq   = freqs  [ i ] ;
		      left  += volume    * sin ( phase + pos * freq ) ;
		      freq  += brainwave * scaling_factor;
		      right += volume    * sin ( phase + pos * freq ) ;
		    }
		}
	    }
	  pos += 1.0 ;
	  if ( fabs ( left ) > maximum ) 
	    maximum = fabs ( left ) ;
	  if ( fabs ( right ) > maximum ) 
	    maximum = fabs ( right ) ;
	  left  *= 32767.0 / maximum;
	  right *= 32767.0 / maximum;
	  buffer [ j ] . leftright . left  = (signed short)left;
	  buffer [ j ] . leftright . right = (signed short)right;
	}
      double total = (++k)*bufsize;
      total/=(double)playrate;
      sprintf(yeah,"%g seconds processed (maximum = %g)" ,total, maximum);
      printf("%s\n", yeah);
      long written = write(output_fd,buffer,4*bufsize);
      assert(written == 4*bufsize);
      printf("Written\n");
    }
  close(output_fd);
  output_fd = -1;
  output_stop = false;
}

int main(int argc, char* argv[])
{
    if (argc>1)
	read_cf(argv[1]);
    else
    {
	fprintf(stderr,"cryosleep <config>\n");
	exit(2);
    }

    printf("Cryosleep v" VERSION " (c) Werner Van Belle, Enrico Weigelt <weigelt@metux.de> 2003, 2007, 2008\n");
    run_cryo_loop();
}
